package com.geekit.Assignment;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class EmployeeFileParserTest {
	private EmployeeFileParser empFileParser;

	@Before
	public void setUp() throws Exception {
		empFileParser = new EmployeeFileParser();
	}

	@Test
	public void parseFileTest() {
		List<Employee> empList = empFileParser.parseFile();
		
		assertEquals(empList.size(), 5);
	}
	
	@Test
	public void addOneEmployeeInfoTest() throws Exception {
		String line = "David,Smith,11-01-1990,Consultant,200000";
		
		empFileParser.addEmployeeInfo(line);
		
		for (Employee e: empFileParser.getEmployeeList()){
			assertEquals(e.getFirstName(), "David");
		}
		
		assertEquals(empFileParser.getEmployeeList().size(), 1);
	}
	
	@Test
	public void addMultipleEmployeeInfoTest() throws Exception {
		String line = "David,Smith,11-01-1991,Consultant,200000";
		String line2 = "Kim,Smith,11-01-1980,Consultant,10000";
		String line3 = "Jason,Smith,11-01-1960,Consultant,150000";
		
		empFileParser.addEmployeeInfo(line);
		empFileParser.addEmployeeInfo(line2);
		empFileParser.addEmployeeInfo(line3);
		
		Employee emp1 = empFileParser.getEmployeeList().get(0);
		Employee emp2 = empFileParser.getEmployeeList().get(1);
		Employee emp3 = empFileParser.getEmployeeList().get(2);
		
		assertEquals(empFileParser.getEmployeeList().size(), 3);
		assertEquals(emp1.getFirstName(), "David");
		assertEquals(emp2.getFirstName(), "Kim");
		assertEquals(emp3.getFirstName(), "Jason");
	}
	
	
	
}
