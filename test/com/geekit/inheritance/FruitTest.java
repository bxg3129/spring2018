package com.geekit.inheritance;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FruitTest {
	Fruit f;

	@Before
	public void setUp() throws Exception {
		System.out.println("I am here");
		f= new RedApple();
		f.setColor("Green");
		f.setShape("Round");
	}
	
	@Test
	public void testGetShape() {
		assertEquals("Round", f.getShape());
	}

	@Test
	public void testGetColor() {
		assertEquals("Green", f.getColor());
	}

}
