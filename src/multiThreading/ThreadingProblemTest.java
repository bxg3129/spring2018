package multiThreading;

public class ThreadingProblemTest {

	public static void main(String[] args) throws InterruptedException {
		ThreadingProblem tp = new ThreadingProblem();
		//ThreadingProblem tp1 = new ThreadingProblem();
		//ThreadingProblem tp2 = new ThreadingProblem();
		
		Thread t1 = new Thread(tp);
		t1.setName("FirstThread");
		
		Thread t2 = new Thread(tp);
		t2.setName("SecondThread");
		
		/*Thread t3 = new Thread(tp2);
		t3.setName("ThirdThread");*/
		
		t1.start();

		t2.start();

		//t3.start();
	}

}
