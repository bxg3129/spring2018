package multiThreading;

public class ThreadingProblem implements Runnable {
	private int numberIncrement;
	private volatile int executionNumber;
	private Object lock1 = new Object();
	
	@Override
	public void run() {
		long initialTime = System.currentTimeMillis();
		for(int i = 0; i < 100; i++){
			//executionNumber++;
			
			increseNumberValue();
			/*System.out.println("New Value : " + numberIncrement + " from thread: " + Thread.currentThread().getName()
					+ "Execution Num: "+ executionNumber);*/
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			System.out.println("New Value: "+ numberIncrement + " from thread: " + Thread.currentThread().getName() );
			
		}
		
		long timeLapse= System.currentTimeMillis() - initialTime; 
		System.out.println("Time to compelte by "+ Thread.currentThread().getName() +" is:"+ timeLapse );
	}

	public synchronized void  increseNumberValue() {
				numberIncrement = numberIncrement + 1;
				//numberIncrement++;
	}

}
