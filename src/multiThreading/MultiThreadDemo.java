package multiThreading;

public class MultiThreadDemo extends Thread {
	private int threadId;
	
	public MultiThreadDemo(int threadId) {
		super();
		this.threadId = threadId;
	}

	public void run(){
		try {
			System.out.println("Thread " +threadId + " is running");
			
			long initialTime = System.currentTimeMillis();
			
			Thread.sleep(1000);
			
			long timeSlept = System.currentTimeMillis() - initialTime; 
			
			System.out.println("Time Slept by thread "+ threadId + " is: "+ timeSlept);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		MultiThreadDemo mt = new MultiThreadDemo(1);
		MultiThreadDemo mt2 = new MultiThreadDemo(2);
		
		//mt.run(); //Single Threaded approach
		//mt2.run(); //Single Threaded approach
		mt.start(); //Multi Threaded approach
		mt2.start(); 
		
		//mt.join();
		
		System.out.println("We just say multithreading exmple");
	}
	
}


