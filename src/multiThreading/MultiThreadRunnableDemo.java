package multiThreading;

public class MultiThreadRunnableDemo implements Runnable{
	private int threadId;
	
	public MultiThreadRunnableDemo(int threadId) {
		super();
		this.threadId = threadId;
	}

	@Override
	public void run() {
		for(int i = 0; i < 10; i++){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Running thread "+ threadId + ". Value is_"+i  );
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		MultiThreadRunnableDemo mt = new MultiThreadRunnableDemo(1);
		MultiThreadRunnableDemo mt2 = new MultiThreadRunnableDemo(2);
		
		Thread t = new Thread(mt);
		Thread t2 = new Thread(mt2);
		
		t.start();
		System.out.println("Running thread " + t.getName());
		t2.start();
		System.out.println("Running thread " + t2.getName());
		
		t.join();
		t2.join();
		
		System.out.println("We saw Runnable approach to create Multi Threading");
	}

}
