package equals.hashcode;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Employee implements Serializable {
	private String name;
	private String address;
	private int SSN;
	private int number;
	protected Date sDate;
	
	public Employee(){
		
	}
	

	public Employee(String name, int sSN) {
		super();
		this.name = name;
		SSN = sSN;
	}


	public Employee(String name, String address, int sSN, int number) {
		super();
		this.name = name;
		this.address = address;
		this.SSN = sSN;
		this.number = number;
	}


	public void studentInfo() {
		System.out.println("Student Name:  " + name + " " + address);
	}


	public String getName() {
		return name;
	}


	public String getAddress() {
		return address;
	}


	public int getSSN() {
		return SSN;
	}


	public int getNumber() {
		return number;
	}
	
	@Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Employee)) {
            return false;
        }
        Employee emp = (Employee) o;
        return SSN == emp.SSN && Objects.equals(name, emp.name);
    }

	@Override
    public int hashCode() {
        return Objects.hash(name, SSN);
    }
}
