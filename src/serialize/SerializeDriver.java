package serialize;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectOutputStream;

public class SerializeDriver {

	public static void main(String[] args) {
		Car car = new Car("Audi", "R8", 2017);
		
		Employee employee = new Employee("David Smith","121 Main St", 222111333, 101, car);

		try {
			FileOutputStream fileOut = new FileOutputStream("employee.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(employee);
			
			//out.flush();
			out.close();
			//fileOut.flush();
			fileOut.close();
			
			
			System.out.printf("Serialized data is saved as employee.ser");
		} catch (IOException i) {
			i.printStackTrace();
		} 
	}

}
