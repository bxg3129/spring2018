package com.geekit.lab02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

public class FileParser {
	List<Employee> employeeList;
	
	public List<Employee> parseFile(File file){
		FileReader fileReader;
		try {
			fileReader = new FileReader(file);

			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			readFileContentAndAddEmployees(bufferedReader);
						
			bufferedReader.close();
		}
		catch (IOException e) {
		// TODO: handle exception
		}
		return employeeList;
	}

	private void readFileContentAndAddEmployees(BufferedReader bufferedReader) throws IOException {
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			addEmployeeInfo(line);
		}		
	}
	
	private void addEmployeeInfo(String line) {
		StringTokenizer st = new StringTokenizer(line, ",");
		
		String firstName = st.nextToken();
		String lastName = st.nextToken();
		String dob = st.nextToken();
		String profession = st.nextToken();
		Double salary = Double.valueOf(st.nextToken());

		Employee emp = new Employee(firstName, lastName, dob, profession, salary);
		
		employeeList.add(emp);
	}

}
