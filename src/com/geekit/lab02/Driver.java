package com.geekit.lab02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class Driver {
	Employee[] employee;

	public static void main(String[] args) {
		File file = new File("employee.txt");

		FileReader fileReader;
		try {
			fileReader = new FileReader(file);

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			int numberOfRecords = 0;
			String string;
			while ((string = bufferedReader.readLine()) != null) {
				numberOfRecords++;
			}

			Driver driver = new Driver();
			driver.createEmployeeArray(numberOfRecords);

			FileReader fileReader2 = new FileReader(file);
			BufferedReader bufferedReader2 = new BufferedReader(fileReader2);
			int counter = 0;
			while ((string = bufferedReader2.readLine()) != null) {
				driver.addEmployeeInfo(string, counter);
				counter++;
			}

			// driver.printEmployeeInformation();

			bufferedReader.close();
			bufferedReader2.close();

			Printer print = new Printer();
			print.print(driver.employee);

			driver.printEmployeesMakingSixFigure();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void printEmployeesMakingSixFigure() {
		// TODO Auto-generated method stub
		Employee[] e = this.employee;
		Employee[] emplMakingSixFigure;

	}

	private void printEmployeeInformation() {
		for (int i = 0; i < employee.length; i++) {
			System.out.println(employee[i]);
		}

	}

	private void addEmployeeInfo(String line, int counter) {
		StringTokenizer st = new StringTokenizer(line, ",");

		Employee emp = new Employee(st.nextToken(), st.nextToken(), st.nextToken(), st.nextToken(),
				Double.valueOf(st.nextToken()));

		employee[counter] = emp;
	}

	private void createEmployeeArray(int numberOfRecords) {
		this.employee = new Employee[numberOfRecords];
	}

}
