package com.geekit.lab02;

import java.util.ArrayList;

public class Printer {
	
	private void printHeader() {
		System.out.println(String.format("|%-15s|%-15s|%-15s|%-15s|%-15s|", "First Name", 
											"Last Name", "DOB", "Profession", "Salary"));
		//print header part
		
	}
	
	public void print(Employee [] employees){
		//add logic here to print employee information
		
		printHeader(); //prints Header Info
		//Access employees one by one
		for(int i = 0; i< employees.length; i++){
			printEmployeeInfo( employees[i]);
		}
		
		printFooter();
	}
	
	public void print(ArrayList<Employee> emplList){
		printHeader();
		
		for(Employee e: emplList){
			printEmployeeInfo(e);
		}
		
		printFooter();
	}

	private void printFooter() {
		// TODO print footer part
		
	}

	private void printEmployeeInfo(Employee emp) {
		// TODO Auto-generated method stub
		System.out.println(String.format("|%-15s|%-15s|%-15s|%-15s|%-15.2f|", emp.getFirstName(), 
				emp.getLastName(), emp.getDateOfBirth(), emp.getProfession(), emp.getSalary()));	
	}

	private class innerClass{
		String someString= "blah";
	}

}
