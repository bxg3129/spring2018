package com.geekit.lab02;

public class Department {
	private Employee supervisor;
	private String name;
	
	public Department() {
		// TODO Auto-generated constructor stub
	}

	public Department(Employee supervisor, String name) {
		super();
		this.supervisor = supervisor;
		this.name = name;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
