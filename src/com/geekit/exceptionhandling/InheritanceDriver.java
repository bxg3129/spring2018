package com.geekit.exceptionhandling;

public class InheritanceDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DarkHorse dh = new DarkHorse();
		dh.setAge(12);
		dh.setName("FirstDarkHorse");
		
		DarkHorse dh1 = new DarkHorse("SecondDarkHorse", 2);
		
		System.out.println(dh1.getSpeed());
		System.out.println(dh.getSpeed());
		
		Horse h = new DarkHorse();
		System.out.println(h.getSpeed());
		System.out.println(h.getColor());
		
	}

}
