package com.geekit.exceptionhandling;

public class SomeRunTimeException extends RuntimeException {
	
	public SomeRunTimeException(String message){
		super(message);
	}
}
