package com.geekit.Assignment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public abstract class FileParser {
	private BufferedReader bufferedReader; 	
	private FileReader fileReader;

	private void createFileReader(File file) throws FileNotFoundException{		
		this.fileReader = new FileReader(file);
	}
	protected BufferedReader createBufferredReader(File file) throws FileNotFoundException{
		createFileReader(file);
		this.bufferedReader = new BufferedReader(fileReader);
		
		return this.bufferedReader;
	}
	
	protected void closeBufferedReader() throws IOException{
		this.bufferedReader.close();
	}
	
	public abstract <T> List<T>  parseFile();
}
