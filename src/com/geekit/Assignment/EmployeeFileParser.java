package com.geekit.Assignment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class EmployeeFileParser extends FileParser{
	private List<Employee> employeeList;
	private File file = new File("employee.txt");
	
	public List<Employee> parseFile(){
		try {
			readFileContentAndAddEmployees(createBufferredReader(this.file));
						
			closeBufferedReader();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return employeeList;
	}
	
	private void readFileContentAndAddEmployees(BufferedReader bufferedReader) throws IOException {
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			addEmployeeInfo(line);
		}		
	}
	
	protected void addEmployeeInfo(String line) {
		if(employeeList == null){
			employeeList = new ArrayList<>();
		}
		
		StringTokenizer st = new StringTokenizer(line, ",");
		
		String firstName = st.nextToken();
		String lastName = st.nextToken();
		String dob = st.nextToken();
		String profession = st.nextToken();
		Double salary = Double.valueOf(st.nextToken());

		Employee emp = new Employee(firstName, lastName, dob, profession, salary);
		
		employeeList.add(emp);
	}

	protected List<Employee> getEmployeeList() {
		return employeeList;
	}
}
