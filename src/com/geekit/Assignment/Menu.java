package com.geekit.Assignment;

import java.util.List;
import java.util.Scanner;

public class Menu {
	public static void main(String[] args) {
		Menu menu = new Menu();

		boolean runMenu = true;
		while (runMenu) {
			menu.displayMenu();

			int userInput = menu.getUserInput().nextInt();

			runMenu = menu.performUserChoice(userInput);

		}
	}

	private boolean performUserChoice(int userInput) {
		boolean redisplayResult = true;
		
		switch (userInput) {
		case 1:
			List<Employee> employeeList = readEmployeeFile();
			
			Printer p = new Printer();
			p.print(employeeList);
			
			break;

		case 2:
			//print employees over 100,000
			System.out.println("You have not implemented this Munu yet");
			
			break;

		default:
			System.out.println("Exiting the system");
			redisplayResult = false;
			break;
		}

		return redisplayResult;
	}

	private List<Employee> readEmployeeFile() {
		EmployeeFileParser empFileParser = new EmployeeFileParser();
		return empFileParser.parseFile();
	}

	public void displayMenu() {
		System.out.println("1. Print All Employees");
		System.out.println("2. Print Employees Over 100,000 Salary");
		System.out.println("Press Anything else to EXIT");
	}

	public Scanner getUserInput() {
		return new Scanner(System.in);
	}

}
