package com.geekit.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CollectionsDemo {
	public static void main(String[] args) {
		Map map1 = new HashMap();
		map1.put(1, "Dave");
		map1.put(1, "Jason");
		map1.put("key", 1.0);
		
		Set keySet = map1.entrySet();
		
		
		Iterator iter = keySet.iterator();
		
		while(iter.hasNext()){
			Map.Entry entry=(Map.Entry)iter.next();
			
			System.out.println("Key: "+entry.getKey() + " Value: "+ entry.getValue());
		}
		
		Map<Object, String> map2 = new LinkedHashMap<>();
		map2.put("CA", "California");
		map2.put("TX", "Texas");
		map2.put("OH", "Ohio");
		map2.put("NY", "New York");
		map2.put("TX", "Tejas");
		map2.put("null", "Some island");
		
		
		Set<Object> mapKeySet = map2.keySet();
		
		for(Object key : mapKeySet){
			System.out.println("Key: "+ key + " Value: "+ map2.get(key));
		}
	}
}
