package com.geekit.inheritance;

public abstract class Apple extends Fruit implements Juicer{

	public Apple() {
		
	}
	
	protected void clean(){
		System.out.println("I am clean now");
	}
	
	@Override
	public void eatThisWay() {
		clean();
		System.out.println("Cut it and take a bite!");
	}
	
	@Override
	public void makeJuice() {
		System.out.println("Here is the juice from "+ this.getClass()+" Class");
		
	}
}
