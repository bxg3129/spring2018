package com.geekit.inheritance;

public class SourApple extends Apple {

	public SourApple() {
		this.color = "Green";
		this.shape="Round";
	}

	public String getColor(){
		return "Purple";
	}

}
