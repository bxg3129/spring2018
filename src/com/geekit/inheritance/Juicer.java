package com.geekit.inheritance;

public interface Juicer {
	public void makeJuice();
}
