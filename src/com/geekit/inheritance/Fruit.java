package com.geekit.inheritance;

public abstract class Fruit {
	protected String color="";
	protected String shape="";
	
	public abstract void eatThisWay();

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getShape() {
		return shape;
	}

	public void setShape(String shape) {
		this.shape = shape;
	}
	
}
