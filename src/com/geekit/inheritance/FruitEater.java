package com.geekit.inheritance;

public class FruitEater {

	public FruitEater() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		Fruit f = new Lychee();
		System.out.println("I am a Lychee with shape: "+ f.getShape() 
		                     + " and color: " + f.getColor()
		                    );
		System.out.println("I eat this way : ");
		f.eatThisWay();
		System.out.println("*****************************************");
		
		Fruit sourApple = new SourApple();
		System.out.println("I am a SourApple with shape: "+ sourApple.getShape() 
        + " and color: " + sourApple.getColor());
		System.out.println("I eat this way : ");
		f.eatThisWay();
		System.out.println("*****************************************");
		
		Fruit redApple = new RedApple();
		System.out.println("I am a RedApple with shape: "+ redApple.getShape() 
        + " and color: " + redApple.getColor());
		System.out.println("I eat this way : ");
		f.eatThisWay();
		
		if(redApple instanceof RedApple){
			RedApple rd = (RedApple) redApple;
			rd.makeJuice();
		}
	}

}
