package com.geekit.inheritance;

public class Lychee extends Fruit {

	public Lychee() {
		this.setColor("Red");
		this.setShape("Round");
	}

	@Override
	public void eatThisWay() {
		System.out.println("Peel the cover.\nDeseed it\nChew n Swallow");
	}

}
