package com.geekit.inheritance;

public class RedApple extends Apple {

	public RedApple() {
		this.color = "Red";
		this.shape="Round";
	}

}
