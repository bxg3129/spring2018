package com.java.service.javasecondweek;

import java.util.ArrayList;
import java.util.List;

//class
class Hello{
	//variables
	//int - data type 
	//number -- variable name
	// number = 0 -- assigning values
	static int number = 0;
	
	//name = null
	String name;
	
	//default constructor
	Hello(){
		/*List<String> s = new ArrayList<String>();
		s.add("someString");
		//s.add(1);
		 */	
		
	}
	
	//overloaded constructor
	Hello(int num){
		number = num;
	}
	
	//static block
	static{
		//instructions goes here
	}
		
	//method
	//Public - visitbility
	//void -- return type
	//main -- method name
	//() -- arguments or parameters or input to the method
	public static void main(String [] args){
		System.out.println("Hello World");
				/// write some instructions
		
		//method call
		System.out.println(add (1, 1));
		
		System.out.println(add (2, 1));
		
		Hello hello1  = new Hello();
		hello1.getNumber(); //Hanna: 0
		
		Hello hello2 = new Hello(100);
		hello2.getNumber();//Hanna: 0
		
		hello1.getNumber();//Rajiv - 0; Hanna - 0;
		
		hello1.loopConcept();
		
		System.out.println("Quotient: "+hello1.divide(2, 10));
		
	}
	
	//add(int a, int b) ==> return a number
	static int add (int a , int b){
		int num = 0;
		
		num = a + b;
		
		return num;
	}
	
	void getNumber(){
		System.out.println("The number is:" + number);
	}
	
	void loopConcept(){
		long temp = 0;
		
		for(int i=2; i<= 100000 ; i++){
			if (isEven(i)){
				temp = temp + i;
			}			
		}
		
		System.out.println("Sum between 1-10 numbers is:" + temp);
	}
	
	boolean isEven(int num){
		if (num % 2 == 0){
			return true;
		}
		else
			return false;
	}
	
	double divide (int divisor, int dividend){
		
		return dividend/divisor;
	}
	
	void boxAction(){
		Integer i = new Integer(1);
		
		int j = i;
		
		Integer k = j;
	}
	
	
	
	

}