package com.java.service.javasecondweek;

public class House {
	int numOfRooms;
	int numWindows;
	double length;
	double width;
	boolean isMediaRoom;
	
	public House() {
		super();
		// TODO Auto-generated constructor stub
	}

	public House(int numOfRooms, int numWindows, double length, double width) {
		super();
		this.numOfRooms = numOfRooms;
		this.numWindows = numWindows;
		this.length = length;
		this.width = width;
	}

	public int getNumOfRooms() {
		return numOfRooms;
	}

	public void setNumOfRooms(int numOfRooms) {
		this.numOfRooms = numOfRooms;
	}

	public int getNumWindows() {
		return numWindows;
	}

	public void setNumWindows(int numWindows) {
		this.numWindows = numWindows;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public boolean isMediaRoom() {
		return isMediaRoom;
	}

	public void setMediaRoom(boolean isMediaRoom) {
		this.isMediaRoom = isMediaRoom;
	}
	
	
}
