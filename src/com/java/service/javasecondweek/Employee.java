package com.java.service.javasecondweek;

import java.util.Date;

public class Employee {
	String name;
	long ssn;
	String address;
	Date dob;

	public Employee(String name, long ssn, String address, Date dob) {
		this.name = name;
		this.ssn = ssn;
		this.address = address;
		this.dob = dob;
	}
	
	public Employee(String name) {
		this.name = name;
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		String s0 = new String("David");
		
		String s1= "David";
		String s2 = "David";
		
		if(s0==s1){
			System.out.println("s0==s1");
		}
		
		if(s2==s1){
			System.out.println("s2==s1");
		}
		
		Employee e1 = new Employee("David");
		Employee e2 = new Employee("David");
		
		if(s1 == e1.name){
			System.out.println("s1==e1.name");
		}
		
		if(s0 == e1.name){
			System.out.println("s0==e1.name");
		}
		
		if (e1 == e2){
			System.out.println("e1==e2");
		}
		
		if(e1.name.equals(e2.name)){
			System.out.println("e1.name.equals(e2.name)");
		}
		
		System.out.println(e1);
		System.out.println(e2);
		
		if(e1.equals(e2)){
			System.out.println("e1.equals(e2)");
		}
				
		String [] strArray = new String[10];
		
		Employee [] empArray = new Employee[10];
		
		int [] integerArray = {1000, 2000, 3000, 4000, 5000};
		
		for(int i = 0; i< integerArray.length; i++){
			System.out.println(integerArray[i]);
		}
	}

}
