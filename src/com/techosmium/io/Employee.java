package com.techosmium.io;

/*
Abiral Pandey
Lab Assignment 1
*/

import java.io.*;
import java.util.*; 
import java.text.NumberFormat;

class Employee {

public static void main(String[] args){

	String fileName = "employee.txt";		//filename shortcut
	String line = null; 					//save each line from file here
	int lineCount = 0; 						//counter to keep track of list iteration

	String fName = "First Name", lName = "Last Name", //column name in variable for formating purpose
			DOBirth = "DOB", Job = "Job", Salary = "Salary";
	
	//creating new arraylist object of employeeData
	ArrayList <EmployeeData> Employees = new ArrayList<EmployeeData>();
	//format to apply '$' in the salary 
	NumberFormat us = NumberFormat.getCurrencyInstance(Locale.US); 
	Scanner scan = new Scanner(System.in);					//creating new scanner object

	try{
		// FileReader reads text files in the default encoding.
		FileReader fileR = new FileReader(fileName); 
		//Always wrap FileReader in BufferedReader.
		BufferedReader bufferedReader = new BufferedReader(fileR);

		menu(); 											//print menu
		int choice = scan.nextInt(); 						//get user option
		top(fName,lName,DOBirth,Job,Salary); 				//print table column name & line 
		while((line = bufferedReader.readLine()) != null){	//read into line until end
			
			String[] tokenz = line.split(", ");	 			//split the phrase using delimiter
			//add element in list by using constructor to initialize newly created object
			Employees.add(new EmployeeData(tokenz[0],tokenz[1],tokenz[2],
											tokenz[3],Double.valueOf(tokenz[4])));
			if(choice > 2) break;		
			if(choice == 1){
				//print the whole employee list
				displayEmployeeList(Employees, lineCount, us);

			}
			else if(choice == 2){
				//print list only with employee salary more than $100,000
				if(Employees.get(lineCount).getSalary() >= 100000.00){
					
					displayEmployeeList(Employees, lineCount, us);
				}
			}
			lineCount++;	//on to the next employee
		}
		
		bottom();			//print table line for bottom

		//close file
		bufferedReader.close(); 

	}
	catch(FileNotFoundException ex){
		System.out.println("Unable to open file!"); 
	}
	catch(IOException ex){
		System.out.println("Error reading file!" );
	}
	finally {
	    if(scan!=null)
	        scan.close();
	}
	
}

public static void top(String fName, String lName, String dob, String job, String salary){
	System.out.print("__________________________________________");
	System.out.print("__________________________________________\n");
	System.out.printf("%-13s | %-13s | %-12s | %-17s | %15s |%n",
						 fName, lName, dob, job, salary );
	System.out.print("__________________________________________");
	System.out.print("__________________________________________\n");
}

public static void bottom(){
	System.out.print("__________________________________________");
	System.out.print("__________________________________________\n");
}

public static void menu(){
	System.out.println("\n\t <> Employer Menu <>");
	System.out.println("------------------------------------------");
	System.out.println(" 1: Display all the Employees Data");
	System.out.println(" 2: Display Employees with +100k Salary\n");
	System.out.print("> ");
}

public static void displayEmployeeList(ArrayList <EmployeeData> Employees, int lineCount, NumberFormat us) {
	
	System.out.printf("%-13s", Employees.get(lineCount).getfName());
	System.out.printf(" | ");
	System.out.printf("%-13s", Employees.get(lineCount).getlName());
	System.out.printf(" | ");
	System.out.printf("%-12s",Employees.get(lineCount).getDateOfBirth()); 
	System.out.printf(" | ");
	System.out.printf("%-17s",Employees.get(lineCount).getJob());
	System.out.printf(" | ");
	System.out.printf("%15s",us.format(Employees.get(lineCount).getSalary())); 
	System.out.printf(" |%n");
}

}
