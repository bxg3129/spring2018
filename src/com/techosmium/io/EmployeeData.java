package com.techosmium.io;

public class EmployeeData {
	private String fName;
	private String lName;
	private String dateOfBirth;
	private String Job;
	private double Salary;

	public EmployeeData(String fName, String lName, String dateOfBirth, String job, double salary) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.dateOfBirth = dateOfBirth;
		this.Job = job;
		this.Salary = salary;
	}
	//Accessors
	public String getfName() {
		return fName;
	}
	//Modifier
	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getJob() {
		return Job;
	}

	public void setJob(String job) {
		Job = job;
	}

	public double getSalary() {
		return Salary;
	}

	public void setSalary(double salary) {
		Salary = salary;
	}
	
}
