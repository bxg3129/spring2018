package advanceforloop;

public class Loop {
	public static void main(String[] args) {
		//Example for For loop
		for (int i = 0; i < 10; i++) {
			System.out.println("Value from For loop-"+i);
		}
		
		//Example of Do While
		int i = 0;
		do{
			System.out.println("Value from Do while loop-"+i);
			i++;
		}while(i< 10);
		
		//While loop
		int j =0;
		while(j<10){
			System.out.println("Value from While loop-"+j);
			j++;
		}
		
		Integer [] intArray = {1,23,34,32,1,2,0,1,2,3};
		for (int k = 0; k < intArray.length; k++) {
			System.out.println("Value from For the Array-"+ intArray[k]);
		}
		
		for (Integer integer : intArray) {
			System.out.println("Value from For the Array using foreach-"+ integer);
		}
		
		
	}
}
